﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.Graphs;
using Microsoft.Azure.Graphs.Elements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace azure_graphdb
{
    class Program
    {
        

        static void Main(string[] args)
        {
            string endpoint = ConfigurationManager.AppSettings["endpoint"];
            string authKey = ConfigurationManager.AppSettings["authkey"];
            var s = new GraphDbRepository(endpoint, authKey, "graphdb", "graphcollz");
            var c = new Contract()
            {
                ContractName = "Test Contract",
                Organization = "Pain in the ass client",
                GenericList = new List<string>(),
                NonGenericList = new ArrayList()
            };
            c.GenericList.Add("Related Item 1");
            c.GenericList.Add("Related Item 2");
            c.NonGenericList.Add("Non-Generic Related Item 1");
            c.NonGenericList.Add("Non-Generic Related Item 2");
            c.NonGenericList.Add("Non-Generic Related Item 3");
            s.Save(c);
        }

        static async Task<int> graphDb()
        {
            string endpoint = ConfigurationManager.AppSettings["endpoint"];
            string authKey = ConfigurationManager.AppSettings["authkey"];
            DocumentClient client = new DocumentClient(new Uri(endpoint), authKey);
            Database database = await client.CreateDatabaseIfNotExistsAsync(new Database { Id = "graphdb" });
            DocumentCollection graphCollection = await client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri("graphdb"),
                new DocumentCollection { Id = "graphcollz" },
                new RequestOptions { OfferThroughput = 1000 }
                );
            // Create a vertex
            IDocumentQuery<Vertex> createVertexQuery = client.CreateGremlinQuery<Vertex>(
                graphCollection,
                "g.addV('person').property('firstName', 'Brian')");

            while (createVertexQuery.HasMoreResults)
            {
                Vertex thomas = (await createVertexQuery.ExecuteNextAsync<Vertex>()).First();
            }
            return 0;
        }

        public class Contract
        {
            public string ContractName { get; set; }
            private string DoNotSave;

            public string Organization { get; set; }
            public List<string> GenericList { get; set; }
            public ArrayList NonGenericList { get; set; }
        }
    }
}
