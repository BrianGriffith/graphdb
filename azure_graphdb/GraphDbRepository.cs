﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.Graphs;
using Microsoft.Azure.Graphs.Elements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace azure_graphdb
{
    public class GraphDbRepository
    {
        private DocumentClient client;
        private Database database;
        private DocumentCollection graphCollection;

        enum GraphObjectType
        {
            Primitive,
            GenericCollection,
            Collection,
            Object
        }

        public GraphDbRepository(string endpoint, string authKey, string databaseName, string collectionName)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            database = client.CreateDatabaseIfNotExistsAsync(new Database { Id = databaseName }).Result;
            graphCollection = client.CreateDocumentCollectionIfNotExistsAsync(database.SelfLink, new DocumentCollection { Id = collectionName }).Result;
        }

        public void Save<T>(T item)
        {
            var objName = item.GetType().Name;
            var objProps = new List<PropertyInfo>(item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance));
            var props = new Dictionary<string, string>();
            
            foreach (var prop in objProps)
            {
                if(prop.PropertyType.IsPrimitive || prop.PropertyType == typeof(string))
                {
                    props.Add(prop.Name, prop.GetValue(item).ToString());
                }
                else if(typeof(ICollection).IsAssignableFrom(prop.PropertyType))
                {
                    //Recursion!
                    var x = prop.GetValue(item, null) as ICollection;
                    foreach (var y in x)
                    {
                        Save(y);
                    }
                }
                //else
                //{
                //    //property is a non-primitive, non-collection object, so need to recurse through it's properties
                //}
            }

            foreach (var property in objProps)
            {
                var graphObjType = DerivePropertyType(property);

                switch (graphObjType)
                {
                    case GraphObjectType.Primitive:
                        props.Add(property.Name, property.GetValue(item).ToString());
                        break;
                    case GraphObjectType.GenericCollection:
                        var genericType = property.PropertyType.GetGenericArguments()[0];

                        break;
                    case GraphObjectType.Collection:
                        break;
                    case GraphObjectType.Object:
                        break;
                    default:
                        break;
                }
            }
            SaveVertex(objName, props);
        }

        private GraphObjectType DerivePropertyType(PropertyInfo prop)
        {
            if (prop.PropertyType.IsPrimitive || prop.PropertyType == typeof(string))
                return GraphObjectType.Primitive;
            if (typeof(ICollection).IsAssignableFrom(prop.PropertyType))
            {
                if (prop.PropertyType.IsGenericType)
                    return GraphObjectType.GenericCollection;
                else
                    return GraphObjectType.Collection;
            }

            return GraphObjectType.Object;
        }

        private void SaveVertex(string vName, Dictionary<string, string> vProps)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("g.AddV('");
            sb.Append(vName);
            sb.Append("')");
            foreach (var vProp in vProps)
            {
                sb.Append(".property('");
                sb.Append(vProp.Key);
                sb.Append("', '");
                sb.Append(vProp.Value);
                sb.Append("')");
            }
            var query = sb.ToString();
            IDocumentQuery<Vertex> v = client.CreateGremlinQuery<Vertex>(graphCollection, query);

            while(v.HasMoreResults)
            {
                var result = v.ExecuteNextAsync<Vertex>().Result;
            }
        }
    }
}
